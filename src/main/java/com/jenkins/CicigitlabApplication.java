package com.jenkins;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CicigitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(CicigitlabApplication.class, args);
	}

}
