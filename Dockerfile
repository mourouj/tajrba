FROM openjdk:8
ADD target/docker-gitlab.jar docker-gitlab.jar
EXPOSE 8088
ENTRYPOINT ["java", "-jar", "docker-gitlab.jar"]